import logo from "./logo.svg";
import "./App.css";
import React, { createContext, useContext, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import ProductList from "./components/ProductList";
import Home from "./components/Home";

import SingleProduct from "./components/SingleProduct";
import MainNavigation from "./components/Navigation/MainNavigation";
import TestPage from "./components/TestPage";

import { DataProvider } from "./components/DataContext";
import Login from "./components/Login";
import Registration from "./components/Registration";

function App() {
  return (
    <Router>
      <DataProvider>
        <MainNavigation />
        {/* <TestPage /> */}
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/signin" element={<Login />} />
            <Route path="/signup" element={<Registration />} />


            <Route path="/product" element={<ProductList />} />
            <Route path="/singleProduct" element={<SingleProduct />} />
          </Routes>
        </main>
      </DataProvider>
    </Router>
  );
}

export default App;
