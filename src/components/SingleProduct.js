import React, { useContext } from "react";
import "./SingleProduct.css";
import { CDN_URL } from "../Constant";
import DataContext from "./DataContext";
const SingleProduct = () => {
  const { data } = useContext(DataContext);

  const {
    cloudinaryImageId,
    name,
    avgRating,
    cuisines,
    costForTwo,
    deliveryTime,
    totalRatingsString,
    availability,
  } = data;

  return (
    <div className="flex-container">
      <div className="box1">
        {" "}
        <img
          className="img-photoss"
          alt="res-logo"
          src={CDN_URL + cloudinaryImageId}
        />
      </div>
      <div className="box2">
        {" "}
        <h3 className="font-bold py-4 text-lg">{name}</h3>
        <h5>{cuisines.join(", ")}</h5>
        <h5>TotalRating:{availability?.opened}</h5>
        <h5>{totalRatingsString}</h5>
        <h5>{avgRating} stars</h5>
        <h5>₹{costForTwo ? costForTwo / 100 + "FOR TWO" : ""} </h5>
        <h5>{deliveryTime} minutes</h5>
        <h5>{deliveryTime} minutes</h5>
      </div>
    </div>
  );
};

export default SingleProduct;
