import React from "react";
import { Link } from "react-router-dom";

import MainHeader from "./MainHeader";
import "./MainNavigation.css";
import NavigationLinks from "./NavigationLinks";
import logo from "../../images/downloadlogo.png";
const MainNavigation = (props) => {
  return (
    <MainHeader>
      <button className="main-navigation__menu-btn">
        <img src={logo} />
      </button>
      <h1 className="main-navigation__title">
        {/* <Link to="/">Yummy</Link> */}
        <img src={logo} />
      </h1>
      <nav>
        <NavigationLinks />
      </nav>
    </MainHeader>
  );
};

export default MainNavigation;
