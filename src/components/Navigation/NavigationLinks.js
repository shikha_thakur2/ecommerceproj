import React from "react";
import { NavLink } from "react-router-dom";

import "./NavigationLinks.css";

const NavigationLinks = (props) => {
  return (
    <ul className="nav-links">
      <li>
        <NavLink to="/" exact>
          HOME
        </NavLink>
      </li>
      <li>
        <NavLink to="/product">MY PRODUCT</NavLink>
      </li>
      <li>
        <NavLink to="/places/new">ADD PRODUCT</NavLink>
      </li>
      <li>{/* <NavLink to="/auth">Profile</NavLink> */}</li>
    </ul>
  );
};

export default NavigationLinks;
