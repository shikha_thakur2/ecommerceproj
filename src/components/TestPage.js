import React, {useEffect, useState} from 'react'


const TestPage =()=>{

    const [buttonColor, setButtonColor] = useState('blue'); // Initial color

    const handleButtonClick = async () => {
      // Change the button color to indicate loading
      setButtonColor('gray');
  
      try {
        // Simulate an API request (replace with your actual API call)
        const response = await fetch('https://api.example.com/data');
        const data = await response.json();
  
        // Update the button color based on API response
        if (data.success) {
          setButtonColor('green'); // Success color
        } else {
          setButtonColor('red'); // Error color
        }
      } catch (error) {
        console.error('Error fetching data:', error);
        setButtonColor('red'); // Error color
      }
    };

    return (


  

    <div>
      <button style={{ backgroundColor: buttonColor }} onClick={handleButtonClick}>
        Fetch Data
      </button>
    </div>
      )


}

export default TestPage;