import React from "react";
import SmartSlider from "react-smart-slider";
import test from "../images/test.webp";
import test1 from "../images/test1.webp";
import test2 from "../images/test2.jpg";

const DummyCaption = ({ caption }) => (
  <div
    style={{
      position: "absolute",
      right: 100,
      top: 250,
      fontSize: 55,
      padding: 55,
      color: "white",
    }}
  >
    {caption}
  </div>
);

const Home = () => {
  const slidesArray = [
    {
      url: test,

      // (Optional) Set if you want to add any content on your slide
      childrenElem: <DummyCaption caption="North Indian Thali" />,
    },
    {
      url: test1,
      childrenElem: <DummyCaption caption="Hyderabadi Biryani" />,
    },
    {
      url: test2,
      childrenElem: <DummyCaption caption="Pasta" />,
    },
  ];
  return (
    <div className="App">
      <h1 style={{ margin: "70px" }}>Get Ready To Taste The Yum!</h1>
      <SmartSlider
        slides={slidesArray}
        buttonShape="square" // round or square
      />
    </div>
  );
};

export default Home;
