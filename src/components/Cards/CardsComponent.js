import { CDN_URL } from "../../Constant";
import "./CardsComponent";
import { useNavigate } from "react-router-dom";
import React, { createContext, useContext, useState } from "react";
import { useData } from "../DataContext";

const CardsComponent = (props) => {
  const history = useNavigate();
  console.log("props", props);
  const { resData } = props;
  console.log("props", props);
  console.log("resData", resData);
  const { data, setData } = useData();
  const {
    cloudinaryImageId,
    name,
    avgRating,
    cuisines,
    costForTwo,
    deliveryTime,
  } = resData;

  const singleProductDetails = (datas) => {
    console.log("data", data);
    history("/singleProduct");
    setData(datas);
  };

  return (
    <div
      className="rest-card"
      style={{
        // backgroundColor: "#D3D3D3",
        color: "black",
      }}
      onClick={() => singleProductDetails(resData)}
    >
      <img
        className="img-photo"
        alt="res-logo"
        src={CDN_URL + cloudinaryImageId}
      />

      <h3 className="font-bold py-4 text-lg">{name}</h3>
      <h5>{cuisines.join(", ")}</h5>
      <h5>{avgRating} stars</h5>
      <h5>₹{costForTwo ? costForTwo / 100 + "FOR TWO" : ""} </h5>
      <h5>{deliveryTime} minutes</h5>
    </div>
  );
};

export default CardsComponent;
