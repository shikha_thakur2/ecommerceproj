import React, { useState, useEffect } from "react";
import CardsComponent from "./Cards/CardsComponent";
import { FoodObject } from "../Constant";

const ProductList = () => {
  const [listofRestaurants, setListofRestaurants] = useState([]);
  const [counter, setCounter] = useState(0);
  const [searchText, setSearch] = useState("");

  useEffect(() => {
    fetchData();
    lambdaFunction();

  }, []);

  const lambdaFunction = async()=>
  {
      
    const jsondata = await fetch(
      "https://r3ucokvra5nzqupxiz6ykuvhqq0uaubf.lambda-url.ap-south-1.on.aws/"
    );
    const json = await jsondata.json();
    console.log("json", json);


  }
  

  const fetchData = async () => {
    console.log("fetch");
    const jsondata = await fetch(
      "https://www.swiggy.com/dapi/restaurants/list/v5?lat=12.9351929&lng=77.62448069999999&page_type=DESKTOP_WEB_LISTING"
    );
    const json = await jsondata.json();
    console.log("json", json);
    const dataaa =
      json?.data?.cards[2]?.card?.card?.gridElements?.infoWithStyle
        ?.restaurants;
    console.log("dataaa", dataaa);

    console.log("FoodObject", FoodObject);
    setListofRestaurants(
      json?.data?.cards[2]?.card?.card?.gridElements?.infoWithStyle?.restaurants
    );
    // setListofRestaurants(FoodObject);
  };

  return listofRestaurants.length === 0 ? null : ( // <Shimmer />
    <>
      <div className="body">
        <div className="restro-container">
          {listofRestaurants.map((restaurant) => (
            <CardsComponent resData={restaurant?.info} />
          ))}
        </div>
      </div>
    </>
  );
};

export default ProductList;
